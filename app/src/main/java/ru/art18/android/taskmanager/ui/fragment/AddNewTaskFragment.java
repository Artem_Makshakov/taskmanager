package ru.art18.android.taskmanager.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.TaskManagerApp;
import ru.art18.android.taskmanager.database.TaskModel;
import ru.art18.android.taskmanager.ui.activity.ChoiceDateActivity;

public class AddNewTaskFragment extends Fragment {

    private static final String TIME_PATTERN = "yyyy-MM-dd ";
    private static final String KEY_TASK_MODEL = "task_model";

    public static AddNewTaskFragment newInstance() {
        AddNewTaskFragment fragment = new AddNewTaskFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    public static AddNewTaskFragment newInstance(TaskModel model) {
        AddNewTaskFragment fragment = new AddNewTaskFragment();
        Bundle arg = new Bundle();
        arg.putParcelable(KEY_TASK_MODEL, model);
        fragment.setArguments(arg);
        return fragment;
    }

    @Bind(R.id.edit_title)
    EditText title;
    @Bind(R.id.edit_description)
    EditText description;
    @Bind(R.id.btn_choose_date)
    Button chooseTime;
    @Bind(R.id.ll_btns)
    View layoutBtns;
    @Bind(R.id.progress)
    ProgressBar progress;
    private TaskModel taskModel;
    @Bind(R.id.time)
    TextView time;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().getParcelable(KEY_TASK_MODEL) != null) {
            taskModel = getArguments().getParcelable(KEY_TASK_MODEL);
        } else {
            taskModel = new TaskModel();
            taskModel.setDate(new Date().getTime());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fr_add_new_task, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        invalidateView();
    }


    private void invalidateView() {
        title.setText(taskModel.getTitle());
        description.setText(taskModel.getDescription());
        Date date = new Date(taskModel.getDate());
        DateFormat outdf = new SimpleDateFormat(TIME_PATTERN, Locale.ENGLISH);
        time.setText(outdf.format(date));
    }

    private void invalidateModel() {
        taskModel.setTitle(title.getText().toString());
        taskModel.setDescription(description.getText().toString());
    }

    @OnClick(R.id.btn_choose_date)
    void onPickDate() {
        ChoiceDateActivity.Run(getActivity());
    }

    public void setDate(long date) {
        invalidateModel();
        taskModel.setDate(date);
        invalidateView();
    }

    @OnClick(R.id.btn_add)
    void onSaveButtinClick() {
        doSaveTask();
    }

    @OnClick(R.id.btn_remove)
    void onRemoveButtinClick() {
        doRemoveTask();
    }

    private void doRemoveTask() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                invalidateModel();
                showProgress();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    TaskManagerApp.getDatabaseHelperHelper().getTaskDao().delete(taskModel);
                } catch (SQLException e) {
                    Log.e(AddNewTaskFragment.class.getSimpleName(), e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                taskModel = new TaskModel();
                invalidateView();
                showData();
            }
        }.execute();

    }

    private void showData() {
        layoutBtns.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        description.setVisibility(View.VISIBLE);
        progress.setVisibility(View.INVISIBLE);
    }

    private void showProgress() {
        layoutBtns.setVisibility(View.INVISIBLE);
        title.setVisibility(View.GONE);
        description.setVisibility(View.INVISIBLE);
        progress.setVisibility(View.VISIBLE);
    }


    private void doSaveTask() {
        new AsyncTask<TaskModel, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                invalidateModel();
                showProgress();
            }

            @Override
            protected Void doInBackground(TaskModel... params) {
                try {
                    if (!taskModel.isEmpty()) {
                        TaskManagerApp.getDatabaseHelperHelper().getTaskDao().createOrUpdate(taskModel);
                    }
                } catch (SQLException e) {
                    Log.e(AddNewTaskFragment.class.getSimpleName(), e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                invalidateView();
                showData();
            }
        }.execute(taskModel);
    }
}
