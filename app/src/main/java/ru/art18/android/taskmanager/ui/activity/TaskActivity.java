package ru.art18.android.taskmanager.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.database.TaskModel;
import ru.art18.android.taskmanager.ui.fragment.AddNewTaskFragment;

public class TaskActivity extends AppCompatActivity {

    private static final String KEY_TASK_MODEL = "task_model_key";

    public static void Run(Context context) {
        Intent intent = new Intent(context, TaskActivity.class);
        context.startActivity(intent);
    }

    public static void Run(Context context, TaskModel model) {
        Intent intent = new Intent(context, TaskActivity.class);
        intent.putExtra(KEY_TASK_MODEL, model);
        context.startActivity(intent);
    }

    AddNewTaskFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_base);

        if (savedInstanceState == null) {
            TaskModel model = getIntent().getParcelableExtra(KEY_TASK_MODEL);

            if (model == null) {
                fragment = AddNewTaskFragment.newInstance();
            } else {
                fragment = AddNewTaskFragment.newInstance(model);
            }
            getSupportFragmentManager().beginTransaction().add(R.id.content, fragment).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            long date = data.getLongExtra(ChoiceDateActivity.KEY_EXTRA_DATE_PICK, 0);
            if (fragment != null) {
                fragment.setDate(date);
            }
        }
    }
}
