package ru.art18.android.taskmanager.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = TaskModel.TABLE_NAME)
public class TaskModel implements Parcelable {

    public static final String TABLE_NAME = "task_model";
    public static final String FIELD_DATE = "date";
    public static final String FIELD_ID = "id";
    public static final String FIELD_STATE = "STATE";

    @Override
    public String toString() {
        return "TaskModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", state=" + state +
                '}';
    }

    public enum State {
        NEW("new"),
        PROGRESS("progress"),
        EXPIRED("expired"),
        FINISH("finish");

        private final String value;

        State(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    @DatabaseField(generatedId = true, columnName = FIELD_ID, allowGeneratedIdInsert = true)
    private long id;
    @DatabaseField(dataType = DataType.STRING)
    private String title;
    @DatabaseField(dataType = DataType.STRING)
    private String description;
    @DatabaseField(dataType = DataType.LONG, columnName = FIELD_DATE)
    private long date;
    @DatabaseField(dataType = DataType.ENUM_STRING, columnName = FIELD_STATE)
    private State state;

    public TaskModel() {
        state = State.NEW;
        //ormLite constructor
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(title) && TextUtils.isEmpty(description);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public long getDate() {
        return date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeLong(this.date);
        dest.writeInt(this.state == null ? -1 : this.state.ordinal());
    }

    protected TaskModel(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.description = in.readString();
        this.date = in.readLong();
        int tmpState = in.readInt();
        this.state = tmpState == -1 ? null : State.values()[tmpState];
    }

    public static final Creator<TaskModel> CREATOR = new Creator<TaskModel>() {
        public TaskModel createFromParcel(Parcel source) {
            return new TaskModel(source);
        }

        public TaskModel[] newArray(int size) {
            return new TaskModel[size];
        }
    };
}
