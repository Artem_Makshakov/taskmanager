package ru.art18.android.taskmanager.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.database.TaskModel;

public class TaskAdapter extends RecyclerView.Adapter<TaskViewHolder> {



    private List<TaskModel> models;
    public TaskAdapter() {
        this.models = new ArrayList<>();
    }

    public TaskAdapter(List<TaskModel> models) {
        this.models = models;
    }

    public void setData(List<TaskModel> modelList) {
        if (modelList != null) {
            models.clear();
            models.addAll(modelList);
            notifyDataSetChanged();
        }
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View taskView = LayoutInflater.from(parent.getContext()).inflate(R.layout.l_item_task, parent, false);

        return new TaskViewHolder(taskView);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        TaskModel model = models.get(position);
        holder.setData(model);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }


}
