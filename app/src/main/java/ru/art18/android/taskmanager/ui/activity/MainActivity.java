package ru.art18.android.taskmanager.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.ui.fragment.DateListFragment;
import ru.art18.android.taskmanager.ui.fragment.ListTaskFragment;
import ru.art18.android.taskmanager.ui.view.SlidingTabLayout;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.progress)
    View mProgress;
    @Bind(R.id.view_pager)
    ViewPager mViewPager;
    @Bind(R.id.sliding_tab)
    SlidingTabLayout tabLayout;

    ViewPagerAdapter mViewPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);
        ButterKnife.bind(this);
        mProgress = findViewById(R.id.progress);
        mViewPager.setOffscreenPageLimit(6);
        mViewPagerAdapter = new ViewPagerAdapter(this, getSupportFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setCurrentItem(mViewPager.getCurrentItem());
        tabLayout.setViewPager(mViewPager);
        resetAdapter();
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return android.R.color.transparent;
            }

            @Override
            public int getDividerColor(int position) {
                return android.R.color.transparent;
            }
        });

    }


    private void showProgress(boolean b) {
        if (mViewPager != null && mProgress != null) {
            mViewPager.setVisibility(b ? View.GONE : View.VISIBLE);
            mProgress.setVisibility(!b ? View.GONE : View.VISIBLE);
        }
    }

    private void resetAdapter() {
        if (mViewPager != null) {
            mViewPagerAdapter.notifyDataSetChanged();
        }
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(Context context, FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ListTaskFragment.newInstance();
                case 1:
                default:
                    return new DateListFragment();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Список";
                case 1:
                default:
                    return "Рсписание";
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            FragmentManager manager = ((Fragment) object).getFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove((Fragment) object);
            trans.commitAllowingStateLoss();
        }
    }
}
