package ru.art18.android.taskmanager.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class TaskDao extends BaseDaoImpl<TaskModel, Integer> {

    protected TaskDao(ConnectionSource connectionSource,
                      Class<TaskModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<TaskModel> getTaskList(long limit, long offset) throws SQLException {
        QueryBuilder<TaskModel, Integer> taskQueryBuilder = queryBuilder();
        taskQueryBuilder.limit(limit);
        taskQueryBuilder.offset(offset);
        return taskQueryBuilder.query();
    }

    public TaskModel getTaskById(long id) throws SQLException {
        QueryBuilder<TaskModel, Integer> taskQueryBuilder = queryBuilder();
        taskQueryBuilder.where().eq(TaskModel.FIELD_ID, id);
        return taskQueryBuilder.queryForFirst();
    }

    public List<TaskModel> getTaskDateInterval(long start, long end, long limit, long offset) throws SQLException {
        QueryBuilder<TaskModel, Integer> taskQueryBuilder = queryBuilder();
        taskQueryBuilder.where().between(TaskModel.FIELD_DATE, start, end);
        taskQueryBuilder.limit(limit);
        taskQueryBuilder.offset(offset);
        return taskQueryBuilder.query();
    }

}