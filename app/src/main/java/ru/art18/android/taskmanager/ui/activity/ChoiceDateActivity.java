package ru.art18.android.taskmanager.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.ui.adapter.MonthViewHolder;
import ru.art18.android.taskmanager.ui.fragment.DateListFragment;

public class ChoiceDateActivity extends AppCompatActivity implements MonthViewHolder.DatePicker {

    public static void Run(Activity context) {
        context.startActivityForResult(new Intent(context, ChoiceDateActivity.class), 1010);
    }

    public static final String KEY_EXTRA_DATE_PICK = "date_pic";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_base);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.content, new DateListFragment()).commit();
        }
    }

    @Override
    public void onDatePick(long date) {
        Intent out = new Intent();
        out.putExtra(KEY_EXTRA_DATE_PICK, date);
        setResult(RESULT_OK, out);
        finish();
    }
}
