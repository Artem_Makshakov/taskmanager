package ru.art18.android.taskmanager.ui.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.TaskManagerApp;
import ru.art18.android.taskmanager.database.TaskModel;
import ru.art18.android.taskmanager.ui.adapter.MonthAdapter;
import ru.art18.android.taskmanager.ui.adapter.MonthViewHolder;

public class DateListFragment extends Fragment {
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.empty_text)
    TextView emptyText;
    @Bind(R.id.progress)
    ProgressBar progressBar;
    private MonthAdapter monthAdapter;
    private MonthViewHolder.DatePicker listtener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MonthViewHolder.DatePicker) {
            listtener = (MonthViewHolder.DatePicker) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        monthAdapter = new MonthAdapter(listtener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fr_date_list, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        list.setAdapter(monthAdapter);
        showProgress();

    }

    private void loadData() {
        new AsyncTask<Void, Void, List<TaskModel>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgress();
            }

            @Override
            protected List<TaskModel> doInBackground(Void... params) {
                List<TaskModel> out = new ArrayList<>();
                try {
                    out.addAll(TaskManagerApp.getDatabaseHelperHelper().getTaskDao().getTaskList(100, 0));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return out;
            }

            @Override
            protected void onPostExecute(List<TaskModel> models) {
                super.onPostExecute(models);
                if (models != null) {
                    monthAdapter.setTaskModels(models);
                    monthAdapter.notifyDataSetChanged();
                }
                showData();
            }
        }.execute();
    }

    private void showData() {
        if (monthAdapter.getItemCount() != 0) {
            emptyText.setVisibility(View.INVISIBLE);
            list.setVisibility(View.VISIBLE);
        } else {
            emptyText.setVisibility(View.VISIBLE);
            list.setVisibility(View.INVISIBLE);
        }
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void showProgress() {
        emptyText.setVisibility(View.INVISIBLE);
        list.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }
}
