package ru.art18.android.taskmanager;

import android.app.Application;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import ru.art18.android.taskmanager.database.DatabaseHelper;

public class TaskManagerApp extends Application {
    private static DatabaseHelper mDatabaseHelper;

    public static synchronized DatabaseHelper getDatabaseHelperHelper() {
        return mDatabaseHelper;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDatabaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mDatabaseHelper = null;
    }
}
