package ru.art18.android.taskmanager.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.database.TaskModel;
import ru.art18.android.taskmanager.ui.activity.TaskActivity;

public class TaskViewHolder extends RecyclerView.ViewHolder {
    private static final String TIME_PATTERN = "yyyy-MM-dd ";

    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.description)
    TextView description;
    @Bind(R.id.time)
    TextView time;
    @Bind(R.id.status)
    View status;
    private TaskModel data;


    public TaskViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(TaskViewHolder.this, itemView);
    }

    @OnClick(R.id.root)
    void onClickItem(View view) {
        TaskActivity.Run(view.getContext(), data);
    }

    @OnClick(R.id.action)
    void onActionClick() {
        data.setState(TaskModel.State.FINISH);
        invalidateState();
    }

    public void setData(TaskModel data) {
        this.data = data;
        initView();
    }

    private void initView() {
        description.setText(data.getDescription());
        title.setText(data.getTitle());

        Date date = new Date(data.getDate());
        Date timeNow = new Date();
        DateFormat outdf = new SimpleDateFormat(TIME_PATTERN, Locale.ENGLISH);
        time.setText(outdf.format(date));
        if (date.before(timeNow)) {
            data.setState(TaskModel.State.EXPIRED);
        }
        invalidateState();
    }

    private void invalidateState() {
        switch (data.getState()) {
            case EXPIRED:
                status.setBackgroundColor(title.getContext().getResources().getColor(android.R.color.holo_red_dark));
                break;
            case FINISH:
                status.setBackgroundColor(title.getContext().getResources().getColor(android.R.color.holo_green_dark));
                break;
            default:
                status.setBackgroundColor(title.getContext().getResources().getColor(android.R.color.white));
        }
    }
}