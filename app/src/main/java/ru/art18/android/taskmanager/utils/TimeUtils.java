package ru.art18.android.taskmanager.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeUtils {
    private static final String TIME_PATTERN_SERVER = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String TIME_PATTERN = "MM";

    public static String parseDate(String input) {
        DateFormat df = new SimpleDateFormat(TIME_PATTERN_SERVER, Locale.ENGLISH);
        Date date = null;
        try {
            date = df.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outdf = new SimpleDateFormat(TIME_PATTERN, Locale.ENGLISH);
        return outdf.format(date);
    }

    public static String getTime() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        SimpleDateFormat format = new SimpleDateFormat(TIME_PATTERN_SERVER, Locale.ENGLISH);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        format.format(date);
        String s = format.format(date);
        return s + "GMT";
    }

    public static boolean canUpdate(String lastRevision) {
        DateFormat df = new SimpleDateFormat(TIME_PATTERN_SERVER, Locale.ENGLISH);
        Date revisionTime = null;
        try {
            revisionTime = df.parse(lastRevision);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        if (revisionTime != null) {
            int newMount = calendar.get(Calendar.MONTH);
            calendar.setTime(revisionTime);
            int revisionMount = calendar.get(Calendar.MONTH);
            return !(newMount == revisionMount);
        }
        return false;
    }
}