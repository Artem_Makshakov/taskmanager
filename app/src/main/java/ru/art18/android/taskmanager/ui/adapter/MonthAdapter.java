package ru.art18.android.taskmanager.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.database.TaskModel;

public class MonthAdapter extends RecyclerView.Adapter<MonthViewHolder> {

    private List<TaskModel> taskModels;
    private MonthViewHolder.DatePicker datePicker;

    public MonthAdapter(MonthViewHolder.DatePicker datePicker) {
        taskModels = new ArrayList<>();
        this.datePicker = datePicker;
    }

    @Override
    public MonthViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.l_month, parent, false);
        return new MonthViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MonthViewHolder holder, int position) {
        Calendar calendar = initCalendar(position);
        holder.Bind(calendar, findTaskByMonth(calendar), datePicker);
    }

    private List<TaskModel> findTaskByMonth(Calendar calendar) {
        List<TaskModel> out = new ArrayList<>();
        Date startDate = new Date(calendar.getTimeInMillis());
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date endDate = new Date(calendar.getTimeInMillis());
        Date date;
        for (TaskModel model : taskModels) {
            date = new Date(model.getDate());
            if (date.after(startDate) && date.before(endDate)) {
                out.add(model);
            }
        }
        return out;
    }

    @NonNull
    private Calendar initCalendar(int month) {
        Calendar calendar = new GregorianCalendar(Locale.ENGLISH);
        calendar.clear(Calendar.MONTH);
        calendar.clear(Calendar.DAY_OF_WEEK);
        calendar.clear(Calendar.DAY_OF_MONTH);

        calendar.set(Calendar.MONTH, month);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar;
    }

    @Override
    public int getItemCount() {
        return 12;
    }

    public void setTaskModels(List<TaskModel> taskModels) {
        this.taskModels.clear();
        this.taskModels.addAll(taskModels);
    }
}
