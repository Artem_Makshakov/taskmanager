package ru.art18.android.taskmanager.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.art18.android.taskmanager.R;
import ru.art18.android.taskmanager.database.TaskModel;

public class MonthViewHolder extends RecyclerView.ViewHolder {

    public interface DatePicker {
        void onDatePick(long date);
    }

    private static final String MONTH_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
    @Bind(R.id.month_name)
    TextView monthName;
    @Bind(R.id.divider)
    View divider;
    @Bind(R.id.first_week)
    LinearLayout firstWeek;
    @Bind(R.id.second_week)
    LinearLayout secondWeek;
    @Bind(R.id.third_week)
    LinearLayout thirdWeek;
    @Bind(R.id.fourth_week)
    LinearLayout fourthWeek;
    @Bind(R.id.fifth_week)
    LinearLayout fifthWeek;
    @Bind(R.id.sixth_week)
    LinearLayout sixthhWeek;

    private List<TaskModel> models;
    private Calendar month;
    private DatePicker datePickerListener;
    private View.OnClickListener onDateClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (datePickerListener != null) {
                month.set(Calendar.DAY_OF_MONTH, (Integer.parseInt(((TextView) v).getText().toString())));
                datePickerListener.onDatePick(month.getTime().getTime());
            }
        }
    };

    public MonthViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(MonthViewHolder.this, itemView);
        fifthWeek.setVisibility(View.GONE);
        sixthhWeek.setVisibility(View.GONE);
    }

    public void Bind(Calendar calendar, List<TaskModel> models, DatePicker datePicker) {
        datePickerListener = datePicker;
        Log.d("models", models.toString());
        month = calendar;
        checkWeekCount(month);
        clearView();
        initView(month);
        initBackground(month, models);

    }

    private void initBackground(Calendar calendar, List<TaskModel> models) {
        for (TaskModel model : models) {
            calendar.clear();
            calendar.setTime(new Date(model.getDate()));
            ViewGroup week = getWeek(calendar.get(Calendar.WEEK_OF_MONTH));
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            if (dayOfWeek == 0) {
                dayOfWeek = 7;
            }
            TextView dayView = (TextView) week.getChildAt(dayOfWeek);
            int count = (int) dayView.getTag();
            count++;
            dayView.setTag(count);
        }

        for (int i = 1; i <= 6; i++) {
            ViewGroup week = getWeek(i);
            for (int d = 1; d < 8; d++) {
                TextView dayView = (TextView) week.getChildAt(d);
                int count = (int) dayView.getTag();
                if (count != 0) {
                    if (count > 0) {
                        dayView.setBackgroundColor(dayView.getContext().getResources().getColor(R.color.color_grin));
                    }
                    if (count > 2) {
                        dayView.setBackgroundColor(dayView.getContext().getResources().getColor(R.color.color_yellow));
                    }
                    if (count > 4) {
                        dayView.setBackgroundColor(dayView.getContext().getResources().getColor(android.R.color.holo_red_dark));
                    }
                }
            }
        }
    }

    private void initView(Calendar calendar) {
        for (int day = 1; day <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); day++) {
            calendar.set(Calendar.DAY_OF_MONTH, day);
            ViewGroup week = getWeek(calendar.get(Calendar.WEEK_OF_MONTH));
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            if (dayOfWeek == 0) {
                dayOfWeek = 7;
            }
            TextView dayView = (TextView) week.getChildAt(dayOfWeek);
            dayView.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            dayView.setOnClickListener(onDateClick);
        }
        Date date = calendar.getTime();
        DateFormat outdf = new SimpleDateFormat(MONTH_PATTERN, Locale.ENGLISH);
        monthName.setText(outdf.format(date));
    }

    private void checkWeekCount(Calendar calendar) {
        if (calendar.getActualMaximum(Calendar.WEEK_OF_MONTH) > 4) {
            fifthWeek.setVisibility(View.VISIBLE);
        } else {
            fifthWeek.setVisibility(View.GONE);
        }
        if (calendar.getActualMaximum(Calendar.WEEK_OF_MONTH) > 5) {
            sixthhWeek.setVisibility(View.VISIBLE);
        } else {
            sixthhWeek.setVisibility(View.GONE);
        }
    }

    private void clearView() {
        for (int i = 1; i <= 6; i++) {
            ViewGroup week = getWeek(i);
            for (int d = 1; d < 8; d++) {
                TextView dayView = (TextView) week.getChildAt(d);
                dayView.setText("");
                dayView.setTag(0);
                dayView.setBackgroundResource(R.drawable.bg_day);

            }
        }
    }

    private ViewGroup getWeek(int week) {
        switch (week) {
            case 1:
                return firstWeek;
            case 2:
                return secondWeek;
            case 3:
                return thirdWeek;
            case 4:
                return fourthWeek;
            case 5:
                return fifthWeek;
            case 6:
                return sixthhWeek;
            default:
                return sixthhWeek;
        }
    }
}
